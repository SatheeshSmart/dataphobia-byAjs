(function () {
    angular.module('app.routes', ['ui.router'])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise('/login');

            $stateProvider
            // Login ========================================
                .state('login', {
                url: '/login',
                templateUrl: 'login.html'
                })
                .state('assesment', {
                url:'/assesment',
                templateUrl:'questionare.html'
                });
        }]);
})();